# Pipeline Deploy

## How to Use

Install the AWS CDK `npm install -g aws-cdk` or `yarn add -g aws-cdk`

Create a new IAM user with the `AdministratorAccess` (This is a temporary solution until a more secure permission list is decided), and add it using `aws configure`

The command `yarn deploy:current` will deploy a stack using the current branch as the stack name

## Todo

- AWS credentials as a config param (in a seperate file?), instead of using the device credentials in `~/.aws`
- Script to Empty S3 bucket before running the `cdk destroy` command for a stack
- Use the values of `outputs.json` to save the distribution url a JIRA ticket, testing index page, etc, and the bucketname for stack destroy
- Bundle into an external package that can be installed as a dependency, and uses a config file in the root.
- Seperate configs for UAT staging links that are permanent

---

**Goal**

To simplify deployment of features to a testing stack so as to develop and test a feature in isolation and reduce regressions when features are added to the staging or production links.

**Idea**

When a pull request for a feature is opened, a CDK script will create an AWS Cloudformation stack with an S3 bucket and a Cloudfront distribution, using the branch name of the feature as the stack name.

Updates to the PR will update the files for that stack only.

Once the feature has been tested, passed code review and merged into master (or declined), the stack is destroyed and removed from the AWS account.

### Step 1 - SDK script

CDK is a framework for creating infrastructure as code for AWS. Our infrasturce is pretty simple, an S3 bucket with a Cloudfront distribution, and our deploys will update the bucket.

To deploy, we run the `cdk deploy` script but pass in the branch name as a process.env. Use this `yarn deploy`

```
BRANCH=ThisIsBranchName cdk deploy --require-approval never
```

(Branch name must go first)

Once the script is completed you will have a new cloudformation stack:

![Cloudformation](docs/cloudformation.png)
![Cloudfront](docs/cloudfront.png)
![S3 Bucket](docs/s3-bucket.png)

This script will create a bucket in static website mode and copy all of the build files from the React build script.

```
const websiteDistSourcePath = './directory-of-build-files'

const sourceBucket = new s3.Bucket(this, "storage-bucket", {
  versioned: true,
  websiteIndexDocument: "index.html",
  publicReadAccess: true,
  removalPolicy: cdk.RemovalPolicy.DESTROY,
});

new BucketDeployment(this, "deploy-website", {
  sources: [Source.asset(websiteDistSourcePath)],
  destinationBucket: sourceBucket,
});
```

Next we create a cloudfront distribution that points to the bucket we just created, and define the custom error pages to redirect (necassary if the app uses a router). We add the comment as the stack name which makes it easier to find in the cloudfront list.

```
new cloudfront.Distribution(this, "cloudfront-distribution", {
  comment: props?.stackName,
  defaultBehavior: { origin: new origins.S3Origin(sourceBucket) },
  errorResponses: [
    {
      httpStatus: 403,
      responseHttpStatus: 200,
      responsePagePath: "/index.html",
    },
    {
      httpStatus: 404,
      responseHttpStatus: 200,
      responsePagePath: "/index.html",
    },
  ],
});
```

The bucket names and cloudfront settings are all the default settings from the library. Because these cloudformations are temporary, and there will be mulitple, we keep them as unique as possible. Eg `Bucket Name -> defaultstack-storagebucket3510e1b1-1vvmhlx8tfzym`

Sign-in URL: https://827557497539.signin.aws.amazon.com/console
User name: conduct_test
Password: @Conduct33
Access Key: AKIA4BLSFJLBWKGY4BWI
Secret Access: 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
