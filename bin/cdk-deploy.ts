#!/usr/bin/env node
import "source-map-support/register";
import * as cdk from "@aws-cdk/core";
import { CdkDeployStack } from "../lib/cdk-deploy-stack";

const BRANCH = process.env.BRANCH || "DefaultStack";
const LOCAL = process.env.LOCAL || "./build";

const app = new cdk.App({ autoSynth: true });
new CdkDeployStack(app, BRANCH, {
  stackName: BRANCH,
  websiteDistSourcePath: LOCAL,
});
