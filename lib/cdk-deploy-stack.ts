import {
  CfnParameter,
  CfnOutput,
  Stack,
  Construct,
  RemovalPolicy,
  StackProps,
} from "@aws-cdk/core";
import { Bucket } from "@aws-cdk/aws-s3";
import { BucketDeployment, Source } from "@aws-cdk/aws-s3-deployment";
import { Distribution } from "@aws-cdk/aws-cloudfront";
import { S3Origin } from "@aws-cdk/aws-cloudfront-origins";

interface DeployStackProps extends StackProps {
  stackName: string;
  websiteDistSourcePath: string;
}
export class CdkDeployStack extends Stack {
  constructor(scope: Construct, id: string, props: DeployStackProps) {
    super(scope, id, props);

    const sourceBucket = new Bucket(this, "storage-bucket", {
      versioned: true,
      websiteIndexDocument: "index.html",
      publicReadAccess: true,
      removalPolicy: RemovalPolicy.DESTROY,
    });
    new CfnOutput(this, "Bucket", { value: sourceBucket.bucketName });

    const distribution = new Distribution(this, "cloudfront-distribution", {
      comment: props.stackName,
      defaultBehavior: { origin: new S3Origin(sourceBucket) },
      errorResponses: [
        {
          httpStatus: 403,
          responseHttpStatus: 200,
          responsePagePath: "/index.html",
        },
        {
          httpStatus: 404,
          responseHttpStatus: 200,
          responsePagePath: "/index.html",
        },
      ],
    });
    new CfnOutput(this, "DistributionId", {
      value: distribution.domainName,
    });

    new BucketDeployment(this, "deploy-website", {
      sources: [Source.asset(props.websiteDistSourcePath)],
      destinationBucket: sourceBucket,
      distribution,
      distributionPaths: ["/*"],
    });
  }
}
